extern crate simple_lsystems;

use simple_lsystems::LSystem;

fn main() {
    let mut system = LSystem::with_axiom("b").unwrap();
    system.add_parsed_rule("a => ab").unwrap();
    system.add_parsed_rule("b => a").unwrap();

    for step in 0..10 {
        println!("Step #{}: {}", step, system.next().unwrap());
    }
}
