extern crate simple_lsystems;
extern crate turtle;

use simple_lsystems::LSystem;
use turtle::Turtle;

fn main() {
    let mut turtle = Turtle::new();

    let mut system = LSystem::with_axiom("L").unwrap();
    system.add_parsed_rule("L => +RF-LFL-FR+").unwrap();
    system.add_parsed_rule("R => -LF+RFR+FL-").unwrap();

    let size = turtle.drawing().size();

    system.nth(5).unwrap();
    system
        .render2d(90f64.to_radians(), (size.width as f64, size.height as f64))
        .rasterize2d(&mut turtle, "blue");
}
