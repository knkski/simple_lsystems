/// Contains lsystem tests. Mixes macros in with regular function calls
/// where appropriate to both demonstrate usage and ensure that they're
/// equivalent.
extern crate simple_lsystems;

use simple_lsystems::LSystem;

// Test that a basic example works
#[test]
fn basic_example() {
    let mut system = LSystem::with_axiom("b").unwrap();
    system.add_parsed_rule("a => ab").unwrap();
    system.add_parsed_rule("b => a").unwrap();

    assert_eq!(format!("{}", system.next().unwrap()), "b");
    assert_eq!(format!("{}", system.next().unwrap()), "a");
    assert_eq!(format!("{}", system.next().unwrap()), "ab");
    assert_eq!(format!("{}", system.next().unwrap()), "aba");
    assert_eq!(format!("{}", system.next().unwrap()), "abaab");
    assert_eq!(format!("{}", system.next().unwrap()), "abaababa");
}

// Tests the weighted functionality. Works consistently by setting the second
// rule to have a probability of 0.
#[test]
fn test_weighted() {
    let mut system = LSystem::with_axiom("FG").unwrap();
    system.add_parsed_rule("F => F+ : 33").unwrap();
    system.add_parsed_rule("F => F- : 0").unwrap();
    system.add_parsed_rule("G => G+ : 42").unwrap();
    system.add_parsed_rule("G => G- : 0").unwrap();

    assert_eq!(format!("{}", system.next().unwrap()), "FG");
    assert_eq!(format!("{}", system.next().unwrap()), "F+G+");
}

// Tests that `LSystem.reset` properly re-initializes the `LSystem`.
#[test]
fn test_reset() {
    let mut system = LSystem::with_axiom("F").unwrap();
    system.add_parsed_rule("F => FF").unwrap();

    assert_eq!(format!("{}", system.next().unwrap()), "F");
    assert_eq!(format!("{}", system.next().unwrap()), "FF");
    assert_eq!(format!("{}", system.next().unwrap()), "FFFF");

    system.reset();

    assert_eq!(format!("{}", system.next().unwrap()), "F");
    assert_eq!(format!("{}", system.next().unwrap()), "FF");
    assert_eq!(format!("{}", system.next().unwrap()), "FFFF");
}

// Tests that parametetric L-systems are properly parsed
#[test]
fn test_parametric() {
    let mut system = LSystem::with_axiom("B(2)A(4, 4)").unwrap();

    system
        .add_parsed_rule("A(x, y) : y <= 3 => A(x * 2, x + y)")
        .unwrap();
    system
        .add_parsed_rule("A(x, y) : y > 3 => B(x)A(x/y, 0)")
        .unwrap();
    system.add_parsed_rule("B(x) : x < 1 => C").unwrap();
    system.add_parsed_rule("B(x) : x >= 1 => B(x - 1)").unwrap();

    assert_eq!(format!("{}", system.next().unwrap()), "B(2)A(4, 4)");
    assert_eq!(format!("{}", system.next().unwrap()), "B(1)B(4)A(1, 0)");
    assert_eq!(format!("{}", system.next().unwrap()), "B(0)B(3)A(2, 1)");
    assert_eq!(format!("{}", system.next().unwrap()), "CB(2)A(4, 3)");
    assert_eq!(format!("{}", system.next().unwrap()), "CB(1)A(8, 7)");
}

// Tests that parametetric L-systems are properly parsed
#[test]
fn test_parametric_2() {
    let mut system = LSystem::with_axiom("A(1)").unwrap();

    system
        .add_parsed_rule("A(s) => F(s)[+A(s/2)][-A(s/2)]")
        .unwrap();

    assert_eq!(format!("{}", system.next().unwrap()), "A(1)");
    assert_eq!(
        format!("{}", system.next().unwrap()),
        "F(1)[+A(0.5)][-A(0.5)]"
    );
    assert_eq!(
        format!("{}", system.next().unwrap()),
        "F(1)[+F(0.5)[+A(0.25)][-A(0.25)]][-F(0.5)[+A(0.25)][-A(0.25)]]"
    );
}

// Tests that a basic context-sensitive example works.
#[test]
fn test_context() {
    let mut system = LSystem::with_axiom("baaaaaaa").unwrap();
    system.add_parsed_rule("b => a").unwrap();
    system.add_parsed_rule("b < a => b").unwrap();

    println!("RULES: {:?}", system.rules());

    assert_eq!(format!("{}", system.next().unwrap()), "baaaaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "abaaaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "aabaaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "aaabaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "aaaabaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "aaaaabaa");
    assert_eq!(format!("{}", system.next().unwrap()), "aaaaaaba");
    assert_eq!(format!("{}", system.next().unwrap()), "aaaaaaab");
    assert_eq!(format!("{}", system.next().unwrap()), "aaaaaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "aaaaaaaa");
}

// TODO: Reenable macro
//// Tests that macro use results in identical system to above test
//#[test]
//fn test_macro_use() {
//    let mut system = LSystem::with_axiom("baaaaaaa");
//    add_rule!(system, 'b' => "a");
//    add_rule!(system, "b" < 'a' => "b");
//
//    assert_eq!(system.next().unwrap(), "baaaaaaa");
//    assert_eq!(system.next().unwrap(), "abaaaaaa");
//    assert_eq!(system.next().unwrap(), "aabaaaaa");
//    assert_eq!(system.next().unwrap(), "aaabaaaa");
//    assert_eq!(system.next().unwrap(), "aaaabaaa");
//    assert_eq!(system.next().unwrap(), "aaaaabaa");
//    assert_eq!(system.next().unwrap(), "aaaaaaba");
//    assert_eq!(system.next().unwrap(), "aaaaaaab");
//    assert_eq!(system.next().unwrap(), "aaaaaaaa");
//    assert_eq!(system.next().unwrap(), "aaaaaaaa");
//}

// Tests that a context-sensitive rule takes precedence over
// a context-free rule.
#[test]
fn test_context_precedence() {
    let mut system = LSystem::with_axiom("abcb").unwrap();
    system.add_parsed_rule("b => x").unwrap();
    system.add_parsed_rule("c < b => y").unwrap();

    assert_eq!(format!("{}", system.next().unwrap()), "abcb");
    assert_eq!(format!("{}", system.next().unwrap()), "axcy");
}

// Tests that a stringified rule is parsed correctly
#[test]
fn test_parse_rule() {
    let mut system = LSystem::with_axiom("baaaaaaa").unwrap();
    system.add_parsed_rule("b => a").unwrap();
    system.add_parsed_rule("b < a => b").unwrap();
    system.add_parsed_rule("a > b => c").unwrap();
    system.add_parsed_rule("c < c > c => d").unwrap();

    assert_eq!(format!("{}", system.next().unwrap()), "baaaaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "abaaaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "cabaaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "ccabaaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "cccabaaa");
    assert_eq!(format!("{}", system.next().unwrap()), "cdccabaa");
    assert_eq!(format!("{}", system.next().unwrap()), "cdcccaba");
    assert_eq!(format!("{}", system.next().unwrap()), "cdcdccab");
    assert_eq!(format!("{}", system.next().unwrap()), "cdcdccca");
    assert_eq!(format!("{}", system.next().unwrap()), "cdcdcdca");
    assert_eq!(format!("{}", system.next().unwrap()), "cdcdcdca");
}

/// Branching and contexts don't work yet.
#[test]
#[should_panic]
fn test_branching_context() {
    let mut system = LSystem::with_axiom("ABC[DE][SG[HI[JK]L]MNO]").unwrap();

    system.add_parsed_rule("BC < S > G[H]M => X").unwrap();

    assert_eq!(
        format!("{}", system.next().unwrap()),
        "ABC[DE][SG[HI[JK]L]MNO]"
    );
    assert_eq!(
        format!("{}", system.next().unwrap()),
        "ABC[DE][XG[HI[JK]L]MNO]"
    );
}
