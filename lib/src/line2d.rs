/// Represents a 2D point
pub type Point = (f64, f64);

#[derive(Debug, Clone)]
/// Represents a 2D line segment
pub struct Line(pub Point, pub Point);

/// Represents a collection of lines, e.g. a drawing
pub struct Lines(pub Vec<Line>);

// It's a lot of boilerplate to attempt to sort a vec of lines, which we need to do
// in order to deduplicate them (which is useful to avoid unnecessary drawing work).
// The boilerplate comes from using f64, which only implements PartialOrd/PartialEq.
// So, we just convert everything to i64's and do the deduping there, then convert
// back. As a bonus, we multiply by 1000, which is equivalent to checking for
// `(a - b).abs() < 0.001`.
impl Line {
    fn to_big_i64(&self) -> ((i64, i64), (i64, i64)) {
        let &Line((x1, y1), (x2, y2)) = self;
        let n = 1000.;

        (
            ((n * x1).round() as i64, (n * y1).round() as i64),
            ((n * x2).round() as i64, (n * y2).round() as i64),
        )
    }

    fn from_big_i64(big: &((i64, i64), (i64, i64))) -> Line {
        let &((x1, y1), (x2, y2)) = big;
        let n = 1000.;

        Line(
            (x1 as f64 / n, y1 as f64 / n),
            (x2 as f64 / n, y2 as f64 / n),
        )
    }
}

impl Lines {
    pub fn rasterize2d<C>(&self, canvas: &mut C, color: &str)
    where
        C: crate::rasterizers::Canvas2D,
    {
        canvas.begin();
        canvas.set_color(color);

        for line in &self.0 {
            canvas.draw_line(line);
        }

        canvas.end();
    }
}

/// Scales line segments and centers around (0, 0)
///
/// Also sorts and deduplicates.
pub fn adjust_lines(lines: &[Line], size: (f64, f64)) -> Vec<Line> {
    let (xmin, xmax, ymin, ymax) = lines.iter().fold(
        (1e9f64, -1e9f64, 1e9f64, -1e9f64),
        |(xmin, xmax, ymin, ymax), &Line((x1, y1), (x2, y2))| {
            (
                xmin.min(x1).min(x2),
                xmax.max(x1).max(x2),
                ymin.min(y1).min(y2),
                ymax.max(y1).max(y2),
            )
        },
    );

    let xadj = (xmax.abs() - xmin.abs()) / 2.;
    let yadj = (ymax.abs() - ymin.abs()) / 2.;

    let xscale = size.0 / (xmax - xmin).max(0.1);
    let yscale = size.1 / (ymax - ymin).max(0.1);

    let scaling = xscale.min(yscale);

    let mut adjusted_lines: Vec<_> = lines
        .iter()
        .map(|Line((x1, y1), (x2, y2))| {
            Line(
                ((x1 - xadj) * scaling, (y1 - yadj) * scaling),
                ((x2 - xadj) * scaling, (y2 - yadj) * scaling),
            )
            .to_big_i64()
        })
        .collect();

    adjusted_lines.sort();
    adjusted_lines.dedup();

    adjusted_lines.iter().map(Line::from_big_i64).collect()
}
