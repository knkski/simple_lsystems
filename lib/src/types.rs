use std::collections::HashMap;
use std::f64::EPSILON;
use std::fmt;
use std::ops::AddAssign;

// ω-related structs

#[derive(Debug, Clone)]
pub struct ActualModule {
    pub letter: char,
    pub parameters: Vec<f64>,
}

impl fmt::Display for ActualModule {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        write!(format, "{}", self.letter)?;

        if !self.parameters.is_empty() {
            write!(
                format,
                "({})",
                self.parameters
                    .iter()
                    .map(|c| c.to_string())
                    .collect::<Vec<_>>()
                    .join(", ")
            )?;
        }

        Ok(())
    }
}

#[derive(Debug, Clone, Default)]
pub struct Word {
    pub modules: Vec<ActualModule>,
}

impl Word {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }
}

impl fmt::Display for Word {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        write!(
            format,
            "{}",
            self.modules
                .iter()
                .map(|m| m.to_string())
                .collect::<Vec<_>>()
                .join("")
        )
    }
}

impl AddAssign for Word {
    fn add_assign(&mut self, other: Word) {
        self.modules.extend(other.modules);
    }
}

// Production-related structs

#[derive(Debug, Clone)]
pub enum Oper {
    Add,
    Sub,
    Mul,
    Div,
}

#[derive(Debug, Clone)]
pub enum RelOper {
    GreaterThan,
    GreaterThanOrEqual,
    LessThan,
    LessThanOrEqual,
    Equal,
}

impl fmt::Display for RelOper {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        use self::RelOper::*;
        match self {
            GreaterThan => write!(format, ">"),
            GreaterThanOrEqual => write!(format, ">="),
            LessThan => write!(format, "<"),
            LessThanOrEqual => write!(format, "<="),
            Equal => write!(format, "="),
        }
    }
}

#[derive(Clone)]
pub enum Expr {
    Value(f64),
    Var(char),
    Add(Box<Expr>, Box<Expr>),
    Sub(Box<Expr>, Box<Expr>),
    Mul(Box<Expr>, Box<Expr>),
    Div(Box<Expr>, Box<Expr>),
    Paren(Box<Expr>),
}

impl Expr {
    fn evaluate(&self, vars: &HashMap<char, f64>) -> f64 {
        use super::types::Expr::*;
        match *self {
            Value(val) => val,
            Var(var) => *vars
                .get(&var)
                .unwrap_or_else(|| panic!("Couldn't get value for {}!", var)),
            Add(ref left, ref right) => left.evaluate(vars) + right.evaluate(vars),
            Sub(ref left, ref right) => left.evaluate(vars) - right.evaluate(vars),
            Mul(ref left, ref right) => left.evaluate(vars) * right.evaluate(vars),
            Div(ref left, ref right) => left.evaluate(vars) / right.evaluate(vars),
            Paren(ref expr) => expr.evaluate(vars),
        }
    }
}

impl fmt::Display for Expr {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        use self::Expr::*;
        match *self {
            Value(val) => write!(format, "{}", val),
            Var(var) => write!(format, "{}", var),
            Add(ref left, ref right) => write!(format, "{} + {}", left, right),
            Sub(ref left, ref right) => write!(format, "{} - {}", left, right),
            Mul(ref left, ref right) => write!(format, "{} * {}", left, right),
            Div(ref left, ref right) => write!(format, "{} / {}", left, right),
            Paren(ref expr) => write!(format, "({})", expr),
        }
    }
}

impl fmt::Debug for Expr {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        use self::Expr::*;
        match *self {
            Value(val) => write!(format, "{}", val),
            Var(var) => write!(format, "{}", var),
            Add(ref left, ref right) => write!(format, "({:?} + {:?})", left, right),
            Sub(ref left, ref right) => write!(format, "({:?} - {:?})", left, right),
            Mul(ref left, ref right) => write!(format, "({:?} * {:?})", left, right),
            Div(ref left, ref right) => write!(format, "({:?} / {:?})", left, right),
            Paren(ref expr) => write!(format, "[{:?}]", expr),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct PredecessorModule {
    pub letter: char,
    pub parameters: Vec<char>,
}

impl fmt::Display for PredecessorModule {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        write!(format, "{}", self.letter)?;

        if !self.parameters.is_empty() {
            write!(
                format,
                "({})",
                self.parameters
                    .iter()
                    .map(|c| c.to_string())
                    .collect::<Vec<_>>()
                    .join(", ")
            )?;
        }

        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct SuccessorModule {
    pub letter: char,
    pub parameters: Vec<Expr>,
}

impl SuccessorModule {
    pub fn evaluate(&self, vars: &HashMap<char, f64>) -> ActualModule {
        ActualModule {
            letter: self.letter,
            parameters: self.parameters.iter().map(|p| p.evaluate(&vars)).collect(),
        }
    }
}

impl fmt::Display for SuccessorModule {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        write!(format, "{}", self.letter)?;

        if !self.parameters.is_empty() {
            write!(
                format,
                "({})",
                self.parameters
                    .iter()
                    .map(|c| c.to_string())
                    .collect::<Vec<_>>()
                    .join(", ")
            )?;
        }

        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Condition {
    pub left: Expr,
    pub operator: RelOper,
    pub right: Expr,
}

impl Condition {
    pub fn evaluate(&self, vars: &HashMap<char, f64>) -> bool {
        let left_side = self.left.evaluate(&vars);
        let right_side = self.right.evaluate(&vars);

        match self.operator {
            RelOper::GreaterThan => left_side > right_side,
            RelOper::GreaterThanOrEqual => left_side >= right_side,
            RelOper::LessThan => left_side < right_side,
            RelOper::LessThanOrEqual => left_side <= right_side,
            RelOper::Equal => (left_side - right_side).abs() < EPSILON,
        }
    }
}

impl fmt::Display for Condition {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        write!(format, "{:?} {} {:?}", self.left, self.operator, self.right)
    }
}

#[derive(Debug, Clone)]
pub struct Production {
    pub path: Vec<PredecessorModule>,
    pub predecessor: PredecessorModule,
    pub tree: Vec<PredecessorModule>,
    pub weight: u32,
    pub conditions: Vec<Condition>,
    pub successors: Vec<SuccessorModule>,
}

impl Production {
    pub fn get_succs(&self, actual: &ActualModule) -> Option<Vec<ActualModule>> {
        let cp = &actual.parameters;
        let pp = &self.predecessor.parameters;

        if actual.letter != self.predecessor.letter {
            return None;
        }

        let same_arg_length = cp.len() == pp.len();

        let vars: HashMap<char, f64> = pp.iter().cloned().zip(cp.iter().cloned()).collect();

        let conditions_pass = self.conditions.iter().all(|c| c.evaluate(&vars));

        if same_arg_length && conditions_pass {
            Some(
                self.successors
                    .iter()
                    .map(|s| s.evaluate(&vars))
                    .collect::<Vec<_>>(),
            )
        } else {
            None
        }
    }
}

impl fmt::Display for Production {
    fn fmt(&self, format: &mut fmt::Formatter) -> fmt::Result {
        if !self.path.is_empty() {
            write!(
                format,
                "{} < ",
                self.path
                    .iter()
                    .map(|p| format!("{}", p))
                    .collect::<Vec<_>>()
                    .join("")
            )?;
        }

        write!(format, "{}", self.predecessor)?;

        if !self.tree.is_empty() {
            write!(
                format,
                " > {}",
                self.tree
                    .iter()
                    .map(|p| format!("{}", p))
                    .collect::<Vec<_>>()
                    .join("")
            )?;
        }

        if !self.conditions.is_empty() {
            write!(
                format,
                " : {}",
                self.conditions
                    .iter()
                    .map(|c| format!("{}", c))
                    .collect::<Vec<_>>()
                    .join("")
            )?;
        }

        write!(
            format,
            " => {}",
            self.successors
                .iter()
                .map(|s| format!("{}", s))
                .collect::<Vec<_>>()
                .join("")
        )
    }
}
