//! A library that makes working with L-systems simple. Works
//! with `char`s as predecessors and `String`s as successors.
//!
//! # Examples
//!
//! ```rust
//! use simple_lsystems::LSystem;
//! let mut system = LSystem::with_axiom("b").unwrap();
//! system.add_parsed_rule("a => ab");
//! ```

extern crate itertools;
extern crate rand;

#[macro_use]
extern crate nom;

#[cfg(feature = "turtle")]
extern crate turtle;

#[cfg(feature = "stdweb")]
extern crate stdweb;

pub mod line2d;
pub mod parsing;
pub mod rasterizers;
pub mod types;

use itertools::{EitherOrBoth, Itertools};
use line2d::{adjust_lines, Line, Lines, Point};
use parsing::{parse_production, parse_word};
use rand::distributions::{Distribution, Weighted, WeightedChoice};
use rand::thread_rng;
use std::f64::consts::PI;
use std::fmt;
use types::{ActualModule, Production, Word};

#[derive(Debug)]
pub enum ParseError {
    Parse(String),
}

impl<'a> From<nom::Err<nom::types::CompleteStr<'a>>> for ParseError {
    fn from(err: nom::Err<nom::types::CompleteStr<'a>>) -> ParseError {
        ParseError::Parse(format!("There was an error while parsing: {:?}", err))
    }
}

impl From<String> for ParseError {
    fn from(err: String) -> ParseError {
        ParseError::Parse(format!("There was an error while parsing: {:?}", err))
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ParseError::Parse(ref err) => write!(f, "ParseError: {}", err),
        }
    }
}

/// Represents a Lindenmayer system
pub struct LSystem {
    /// The starting value for the L-system
    axiom: Word,

    /// The current state of the L-system
    state: Option<Word>,

    /// The L-system's set of productions. Each production is stored
    /// as a tuple with optional predecessor/successors for the production.
    rules: Vec<Production>,
}

impl LSystem {
    pub fn state(&self) -> Option<Word> {
        self.state.clone()
    }

    pub fn rules(&self) -> &Vec<Production> {
        &self.rules
    }

    /// Creates a new L-system with the given axiom
    pub fn with_axiom(axiom: &str) -> Result<LSystem, ParseError> {
        let (remainder, axiom) = parse_word(axiom.into())?;

        if !remainder.is_empty() {
            return Err(format!("Found remainder while parsing axiom: {}", remainder).into());
        }

        Ok(LSystem {
            axiom,
            state: None,
            rules: Vec::new(),
        })
    }

    pub fn add_parsed_rule(&mut self, rule: &str) -> Result<(), ParseError> {
        let stripped = rule.replace(" ", "");
        let (remainder, production) = parse_production(stripped.as_str().into())?;

        if !remainder.is_empty() {
            return Err(format!("Found remainder while parsing rule: {}", remainder).into());
        }

        self.rules.push(production);

        Ok(())
    }

    /// Resets the current state of the L-system so that the
    /// next state generated is equal to the axiom.
    pub fn reset(&mut self) {
        self.state = None;
    }

    /// Gets the successor for a particular module
    fn get_successor(
        &self,
        current: &ActualModule,
        path: &[ActualModule],
        tree: &[ActualModule],
    ) -> Word {
        let mut matching_succs = self
            .rules
            .iter()
            .filter_map(|p| {
                let path_matches =
                    p.path
                        .iter()
                        .zip_longest(path)
                        .fold(true, |memo, either_or_both| match either_or_both {
                            EitherOrBoth::Both(a, b) => {
                                memo && a.letter == b.letter
                                    && a.parameters.len() == b.parameters.len()
                            }
                            EitherOrBoth::Left(_) => false,
                            EitherOrBoth::Right(_) => memo,
                        });

                let tree_matches =
                    p.tree
                        .iter()
                        .zip_longest(tree)
                        .fold(true, |memo, either_or_both| match either_or_both {
                            EitherOrBoth::Both(a, b) => {
                                memo && a.letter == b.letter
                                    && a.parameters.len() == b.parameters.len()
                            }
                            EitherOrBoth::Left(_) => false,
                            EitherOrBoth::Right(_) => memo,
                        });

                if !path_matches || !tree_matches {
                    return None;
                }

                match p.get_succs(current) {
                    Some(succs) => {
                        Some((p.weight, !p.path.is_empty() || !p.tree.is_empty(), succs))
                    }
                    None => None,
                }
            })
            .collect::<Vec<_>>();

        let has_context_rule = matching_succs
            .iter()
            .fold(false, |memo, &(_, is_context, _)| memo || is_context);

        if has_context_rule {
            matching_succs = matching_succs
                .into_iter()
                .filter(|&(_, is_context, _)| is_context)
                .collect::<Vec<_>>()
        }

        if matching_succs.is_empty() {
            return Word {
                modules: vec![current.clone()],
            };
        }

        // Each matching rule will have a weight associated with it. Do some mangling to
        // let `WeightedChoice` do the heavy lifting of selecting a weighted random choice.
        let items = &mut matching_succs
            .into_iter()
            .map(|(weight, _, succs)| Weighted {
                weight,
                item: succs,
            })
            .collect::<Vec<_>>();

        let weighted = WeightedChoice::new(items);

        let mut rng = thread_rng();
        let choice = weighted.sample(&mut rng);

        Word { modules: choice }
    }

    /// Renders the system's state to a series of line segments
    ///
    /// Interprets the current state as turtle graphics commands
    pub fn render2d(&self, angle_delta: f64, size: (f64, f64)) -> Lines {
        let mut position = (0., 0.);
        // Ends up getting scaled anyways, so particular value doesn't matter
        let mut angle = PI / 2.;
        let mut tower: Vec<(Point, f64)> = vec![];
        let mut lines = vec![];

        match &self.state {
            Some(ref state) => {
                for module in &state.modules {
                    match module.letter {
                        'F' => {
                            let distance = module.parameters.get(0).unwrap_or(&1.0);

                            let new_position = (
                                position.0 + distance * angle.cos(),
                                position.1 + distance * angle.sin(),
                            );
                            lines.push(Line(position, new_position));
                            position = new_position;
                        }
                        'f' => {
                            let distance = module.parameters.get(0).unwrap_or(&1.0);

                            position = (
                                position.0 + distance * angle.cos(),
                                position.1 + distance * angle.sin(),
                            );
                        }
                        '+' => angle += module.parameters.get(0).unwrap_or(&angle_delta),
                        '-' => angle -= module.parameters.get(0).unwrap_or(&angle_delta),
                        '[' => tower.push((position, angle)),
                        ']' => {
                            let (point, a) = tower.pop().unwrap();
                            position = point;
                            angle = a;
                        }
                        _ => {}
                    }
                }

                Lines(adjust_lines(&lines, size))
            }
            None => Lines(vec![]),
        }
    }
}

impl Iterator for LSystem {
    type Item = Word;

    fn next(&mut self) -> Option<Self::Item> {
        let mut next = Word::new();

        self.state = match self.state {
            Some(ref s) => {
                let modules = s.modules.clone();

                // Loop through each character in the current state
                // and find a replacement, or replace it with itself.
                for (i, current) in modules.iter().enumerate() {
                    let (path, tree) = modules.split_at(i);
                    let mut path = path.to_vec();
                    path.reverse();

                    next += self.get_successor(current, &path, &tree[1..]);
                }

                Some(next)
            }
            None => Some(self.axiom.clone()),
        };

        self.state.clone()
    }
}
