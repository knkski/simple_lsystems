use nom::types::CompleteStr;
use nom::{alpha, anychar, multispace, recognize_float, IResult};
use std::ops::{Range, RangeFrom, RangeTo};
use std::str::FromStr;
use crate::types::{
    ActualModule, Condition, Expr, Oper, PredecessorModule, Production, RelOper, SuccessorModule,
    Word,
};

// ω-related parsing

named!(actual_module<CompleteStr, ActualModule>,
    do_parse!(
        letter: anychar >>
        parameters: opt!(complete!(delimited!(
            delimited!(opt!(multispace), tag!("("), opt!(multispace)),
            separated_list!(delimited!(opt!(multispace), tag!(","), opt!(multispace)), floating_point),
            delimited!(opt!(multispace), tag!(")"), opt!(multispace))
        ))) >>

        (ActualModule { letter, parameters: parameters.unwrap_or_else(Vec::new) })
    )
);

named!(actual_modules<CompleteStr, Vec<ActualModule>>, many0!(complete!(actual_module)));

named!(pub parse_word<CompleteStr, Word>, do_parse!(modules: actual_modules >> (Word { modules })));

// Production-related parsing

// `double_s` doesn't work with `CompleteStr`'s, see
// https://github.com/Geal/nom/issues/809 and
// https://github.com/Geal/nom/issues/815
#[allow(unused_imports)]
pub fn floating_point<I, O, U>(input: I) -> IResult<I, O>
where
    I: Clone
        + nom::Slice<Range<usize>>
        + nom::Slice<RangeFrom<usize>>
        + nom::Slice<RangeTo<usize>>
        + nom::Offset
        + nom::InputIter<Item = U, RawItem = U>
        + nom::AtEof
        + nom::InputLength
        + nom::InputTake
        + nom::ParseTo<O>
        + nom::InputTakeAtPosition<Item = U>,
    U: nom::AsChar,
{
    flat_map!(input, call!(recognize_float), parse_to!(O))
}

fn fold_exprs(initial: Expr, remainder: Vec<(Oper, Expr)>) -> Expr {
    remainder.into_iter().fold(initial, |acc, pair| {
        let (oper, expr) = pair;
        match oper {
            Oper::Add => Expr::Add(Box::new(acc), Box::new(expr)),
            Oper::Sub => Expr::Sub(Box::new(acc), Box::new(expr)),
            Oper::Mul => Expr::Mul(Box::new(acc), Box::new(expr)),
            Oper::Div => Expr::Div(Box::new(acc), Box::new(expr)),
        }
    })
}

named!(factor<CompleteStr, Expr>, alt_complete!(
    map!(
        delimited!(opt!(multispace), alt!(floating_point), opt!(multispace)),
      Expr::Value
    )
    |
    map!(
      map_res!(
        delimited!(opt!(multispace), alt!(alpha), opt!(multispace)),
        |s: CompleteStr| { FromStr::from_str(&s.to_string()) }
      ),
      Expr::Var
    )
    |
    parens
  )
);

named!(term<CompleteStr, Expr>, do_parse!(
    initial: factor >>
    remainder: many0!(
        alt!(
            do_parse!(tag!("*") >> mul: factor >> (Oper::Mul, mul)) |
            do_parse!(tag!("/") >> div: factor >> (Oper::Div, div))
        )) >>
    (fold_exprs(initial, remainder))
));

named!(expr<CompleteStr, Expr>, do_parse!(
    initial: term >>
    remainder: many0!(
    alt!(
        do_parse!(tag!("+") >> add: term >> (Oper::Add, add)) |
        do_parse!(tag!("-") >> sub: term >> (Oper::Sub, sub))
    )) >>
    (fold_exprs(initial, remainder))
));

named!(parens<CompleteStr, Expr>, delimited!(
    delimited!(opt!(multispace), tag!("("), opt!(multispace)),
    map!(map!(expr, Box::new), Expr::Paren),
    delimited!(opt!(multispace), tag!(")"), opt!(multispace))
  )
);

named!(params<CompleteStr, Vec<Expr>>, delimited!(
    delimited!(opt!(multispace), tag!("("), opt!(multispace)),
    separated_list!(tag!(","), expr),
    delimited!(opt!(multispace), tag!(")"), opt!(multispace))
  )
);

named!(successor_module<CompleteStr, SuccessorModule>,
    do_parse!(
        letter: none_of!(":=<>|") >>
        parameters: opt!(complete!(params)) >>

        (SuccessorModule { letter, parameters: parameters.unwrap_or_else(Vec::new) })
    )
);

named!(successor_modules<CompleteStr, Vec<SuccessorModule>>, many0!(complete!(successor_module)));

named!(predecessor_module<CompleteStr, PredecessorModule>,
    do_parse!(
        letter: none_of!(":=<>|") >>
        parameters: opt!(complete!(delimited!(
            delimited!(opt!(multispace), tag!("("), opt!(multispace)),
            separated_list!(delimited!(opt!(multispace), tag!(","), opt!(multispace)), anychar),
            delimited!(opt!(multispace), tag!(")"), opt!(multispace))
        ))) >>

        (PredecessorModule { letter, parameters: parameters.unwrap_or_else(Vec::new) })
    )
);

named!(predecessor_modules<CompleteStr, Vec<PredecessorModule>>, many0!(complete!(predecessor_module)));

named!(condition<CompleteStr, Condition>,
    do_parse!(
        left: expr >>
        operator: delimited!(opt!(multispace), alt!(
            // Order matters here apparently. Attempts to parse `>` before `>=` and blows up
            do_parse!(tag!(">=") >> (RelOper::GreaterThanOrEqual)) |
            do_parse!(tag!(">") >> (RelOper::GreaterThan)) |
            do_parse!(tag!("<=") >> (RelOper::LessThanOrEqual)) |
            do_parse!(tag!("<") >> (RelOper::LessThan)) |
            do_parse!(tag!("=") >> (RelOper::Equal))
        ), opt!(multispace)) >>
        right: expr >>
        (Condition { left, operator, right })
    )
);

named!(conditions<CompleteStr, Vec<Condition>>, separated_list!(tag!("&"), condition));

named!(pub parse_production<CompleteStr, Production>,
    do_parse!(
        path: opt!(terminated!(predecessor_modules, tag!("<"))) >>
        predecessor: predecessor_module >>
        tree: opt!(preceded!(tag!(">"), predecessor_modules)) >>
        conditions: opt!(preceded!(tag!(":"), conditions)) >>
        tag!("=>") >>
        successors: opt!(successor_modules) >>
        weight: opt!(preceded!(tag!(":"), floating_point)) >>

        (Production {
            path: path.unwrap_or_else(Vec::new),
            predecessor,
            tree: tree.unwrap_or_else(Vec::new),
            weight: weight.unwrap_or(100),
            conditions: conditions.unwrap_or_else(Vec::new),
            successors: successors.unwrap_or_else(Vec::new),
        })
    )
);
