use crate::line2d::Line;
use crate::rasterizers::Canvas2D;
use stdweb::web::CanvasRenderingContext2d;

impl Canvas2D for CanvasRenderingContext2d {
    fn begin(&mut self) {
        self.begin_path();
    }

    fn set_color(&mut self, color: &str) {
        self.set_stroke_style_color(color);
    }

    fn draw_line(&mut self, line: &Line) {
        self.move_to(line.0 .0, line.0 .1);
        self.line_to(line.1 .0, line.1 .1);
    }

    fn end(&mut self) {
        self.stroke();
        self.close_path();
    }
}
