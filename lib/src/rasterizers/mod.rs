#[cfg(feature = "stdweb")]
pub mod canvas;

#[cfg(feature = "turtle")]
pub mod turtle;

pub mod hpgl;

use crate::Line;

pub trait Canvas2D {
    fn begin(&mut self) {}
    fn set_color(&mut self, _color: &str) {}
    fn draw_line(&mut self, _line: &Line) {}
    fn end(&mut self) {}
}
