use crate::line2d::Line;
use crate::rasterizers::Canvas2D;
use turtle::Turtle;

impl Canvas2D for Turtle {
    fn begin(&mut self) {
        self.reset();
        self.hide();
        self.set_speed("instant");
    }

    fn set_color(&mut self, color: &str) {
        self.set_pen_color(color);
    }

    fn draw_line(&mut self, line: &Line) {
        self.pen_up();
        self.go_to((line.0 .0, line.0 .1));
        self.turn_towards((line.1 .0, line.1 .1));
        self.pen_down();
        self.forward(((line.1 .0 - line.0 .0).powi(2) + (line.1 .1 - line.0 .1).powi(2)).sqrt());
        self.pen_up();
    }
}
