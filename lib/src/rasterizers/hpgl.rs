use crate::line2d::Line;
use crate::rasterizers::Canvas2D;

#[derive(Default)]
pub struct HPGL {
    pub result: Vec<String>,
}

impl HPGL {
    pub fn new() -> HPGL {
        HPGL { result: vec![] }
    }
}

impl Canvas2D for HPGL {
    fn begin(&mut self) {
        self.result.push("IN;".into());
        self.result.push("SP1;".into());
    }

    fn draw_line(&mut self, line: &Line) {
        self.result.push("PU;".into());
        self.result
            .push(format!("PA{},{};", line.0 .0 as i64, line.0 .1 as i64));
        self.result.push("PD;".into());
        self.result
            .push(format!("PA{},{};", line.1 .0 as i64, line.1 .1 as i64));
    }
}
