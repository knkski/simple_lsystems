#![recursion_limit = "1024"]

/// Draws L-systems using webassembly
/// Some good sources of L-systems:
///  - http://mathforum.org/advanced/robertd/lsys2d.html
///  - http://www.kevs3d.co.uk/dev/lsystems/
#[macro_use]
extern crate stdweb;

mod presets;

use simple_lsystems::rasterizers::hpgl::HPGL;
use simple_lsystems::LSystem;
use stdweb::traits::*;
use stdweb::unstable::TryInto;
use stdweb::web::html_element::CanvasElement;
use stdweb::web::{document, CanvasRenderingContext2d};
use yew::prelude::*;

/// Represents the app
///
/// Keeps track of the current L-system and the list of presets
struct Model {
    link: ComponentLink<Self>,
    axiom: String,
    rules: Vec<String>,
    angle: f64,
    iters: u32,
    color: String,
    presets: Vec<presets::Preset>,
    hpgl: String,
}

/// Represents a message that the app responds to
pub enum Msg {
    Draw,
    AxiomChange(String),
    RulesChange(String),
    AngleChange(String),
    ItersChange(String),
    ColorChange(String),
    Preset(usize),
    DrawHPGL,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let presets = presets::get_presets();
        let default = presets[0].clone();

        Model {
            link,
            axiom: default.axiom,
            rules: default.rules,
            angle: default.angle,
            iters: default.iters,
            color: default.color,
            presets,
            hpgl: "".into(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            // Does all of the heavy lifting.
            // Other update handlers proxy off to this one to actually update the drawing.
            Msg::Draw => {
                // Get canvas element and clear it
                let canvas: CanvasElement = document()
                    .query_selector("#canvas")
                    .unwrap()
                    .unwrap()
                    .try_into()
                    .unwrap();
                let mut context: CanvasRenderingContext2d = canvas.get_context().unwrap();
                let rect = canvas.get_bounding_client_rect();
                let height = rect.get_height();
                let width = rect.get_width();

                context.restore();
                context.save();
                context.set_fill_style_color("#F5F7FA");
                context.clear_rect(0., 0., width * 2., height * 2.);

                // Create lsystem and generate with it
                let mut system = match LSystem::with_axiom(&self.axiom.clone()) {
                    Ok(s) => s,
                    Err(err) => {
                        console!(log, format!("Error parsing rule: {}", err));
                        return false;
                    }
                };

                for rule in &self.rules {
                    system
                        .add_parsed_rule(rule)
                        .unwrap_or_else(|_| console!(log, format!("Error parsing rule: {}", rule)));
                }

                system.nth(self.iters as usize).unwrap();

                let mut lines =
                    system.render2d(self.angle.to_radians(), (width * 0.9, height * 0.9));

                // The lines are centered around (0, 0) and has a vertical axis that points upwards.
                // Move the lines to the canvas' origin and flip the vertical axis to correct it.
                let xmid = width / 2.;
                let ymid = height / 2.;

                for line in &mut lines.0 {
                    line.0 .0 += xmid;
                    line.1 .0 += xmid;
                    line.0 .1 = -line.0 .1 + ymid;
                    line.1 .1 = -line.1 .1 + ymid;
                }

                // Do the thing
                lines.rasterize2d(&mut context, &self.color);
                //                rasterize(&context, lines, &self.color);
            }
            Msg::AxiomChange(ax) => {
                self.axiom = ax;
                self.update(Msg::Draw);
            }
            Msg::RulesChange(r) => {
                self.rules = r.split('\n').map(|s| s.to_owned()).collect();
                self.update(Msg::Draw);
            }
            Msg::AngleChange(a) => {
                self.angle = a.parse::<f64>().unwrap_or(self.angle);
                self.update(Msg::Draw);
            }
            Msg::ItersChange(it) => {
                self.iters = it.parse::<u32>().unwrap_or(self.iters);
                self.update(Msg::Draw);
            }
            Msg::ColorChange(c) => {
                self.color = c;
                self.update(Msg::Draw);
            }
            Msg::Preset(i) => {
                let p = &self.presets[i].clone();
                self.axiom = p.axiom.clone();
                self.rules = p.rules.clone();
                self.angle = p.angle;
                self.iters = p.iters;
                self.color = p.color.clone();
                self.update(Msg::Draw);
            }
            Msg::DrawHPGL => {
                let mut system = match LSystem::with_axiom(&self.axiom.clone()) {
                    Ok(s) => s,
                    Err(err) => {
                        console!(log, format!("Error parsing rule: {}", err));
                        return true;
                    }
                };

                for rule in &self.rules {
                    system
                        .add_parsed_rule(rule)
                        .unwrap_or_else(|_| console!(log, format!("Error parsing rule: {}", rule)));
                }

                system.nth(self.iters as usize).unwrap();

                // Inches to mm to plotter dots, then add a bit of border
                let width = 18f64 * 25.4 * 40. * 0.9;
                let height = 23f64 * 25.4 * 40. * 0.9;

                let mut hpgl = HPGL::new();
                system
                    .render2d(self.angle.to_radians(), (width, height))
                    .rasterize2d(&mut hpgl, "ignored");

                self.hpgl = format!(
                    "data:text/plain;base64,{}",
                    base64::encode(&hpgl.result.join("\n"))
                );
            }
        }
        true
    }
    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }
    fn view(&self) -> Html {
        html! {
            <div id="content" class="container-fluid">
                <div class="row">
                    <div id="control-panel" class="form-group col-sm">
                        <label for="axiom">{ "Axiom" }</label>
                        <input
                            type="text"
                            id="axiom"
                            class="form-control"
                            value=&self.axiom
                            oninput=self.link.callback(|e: yew::InputData| Msg::AxiomChange(e.value))
                        />

                        <label for="rules">{ "Rules" }</label>
                        <textarea
                            id="rules"
                            class="form-control"
                            value=&self.rules.join("\n")
                            oninput=self.link.callback(|e: yew::InputData| Msg::RulesChange(e.value))
                        ></textarea>

                        <label for="angle">{ "Angle" }</label>
                        <input
                            type="number"
                            id="angle"
                            class="form-control"
                            step="0.1"
                            value=&self.angle
                            oninput=self.link.callback(|e: yew::InputData| Msg::AngleChange(e.value))
                        />

                        <label for="iterations">{ "Iterations" }</label>
                        <input
                            type="number"
                            id="iterations"
                            class="form-control"
                            min="0"
                            value=&self.iters
                            oninput=self.link.callback(|e: yew::InputData| Msg::ItersChange(e.value))
                        />

                        <label for="color">{ "Color" }</label>
                        <input
                            type="color"
                            id="color"
                            class="form-control"
                            value=&self.color
                            oninput=self.link.callback(|e: yew::InputData| Msg::ColorChange(e.value))
                        />

                        <br/>
                        <button id="draw" class="btn btn-primary" onclick=self.link.callback(|_| Msg::Draw)>{ "Draw" }</button>
                        <a href=self.hpgl.clone() download="output.plt" onmousedown=self.link.callback(|_| Msg::DrawHPGL)>
                            <button id="hpgl-download" class="btn btn-info" style="width: 100%;">{ "Download as HPGL" }</button>
                        </a>
                        // TODO: These buttons
                        /*<div id="extra-buttons",>
                            <button id="export", class="btn btn-info",>{ "Export" }</button>
                            <button id="share", class="btn btn-info",>{ "Get Link" }</button>
                        </div>*/
                    </div>
                    <div id="canvas-wrapper" class="col-sm">
                        <canvas id="canvas" height="768" width="768"></canvas>
                    </div>
                    <div id="presets">
                        <h3 class="presets-title">{"Presets:"}</h3>
                        <div class="preset-wrapper">
                            { for self.presets.iter().enumerate().map(|(i, p)| (self, i, p.name.clone())).map(view_preset) }
                        </div>
                    </div>
                </div>
            </div>
        }
    }
}

/// Individual preset view
///
/// It's difficult to send a message that contains complex data structures (e.g. Strings),
/// so send a message with the index of the desired preset instead.
fn view_preset((model, idx, name): (&Model, usize, String)) -> Html {
    html! {
        <div
            class="option card"
            onclick=model.link.callback(move |_| Msg::Preset(idx))
        >
            <div class="card-body">
                <span class="card-title">{ name }</span>
            </div>
        </div>
    }
}

fn main() {
    yew::initialize();
    App::<Model>::new().mount_to_body().send_message(Msg::Draw);
    yew::run_loop();
}
