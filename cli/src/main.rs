#[macro_use]
extern crate clap;
#[macro_use]
extern crate failure;
extern crate simple_lsystems;

use clap::{App, Arg};
use failure::Error;
use simple_lsystems::rasterizers::hpgl::HPGL;
use simple_lsystems::LSystem;

arg_enum! {
    #[derive(Debug)]
    #[allow(non_camel_case_types)]
    pub enum Format {
        text,
        hpgl,
    }
}

fn run() -> Result<(), Error> {
    let matches = App::new("myapp")
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .args(&[
            Arg::with_name("axiom")
                .short("w")
                .long("axiom")
                .takes_value(true)
                .help("The axiom to generate the L-system with"),
            Arg::with_name("rules")
                .short("r")
                .long("rule")
                .takes_value(true)
                .multiple(true)
                .help("An L-system rule"),
            Arg::with_name("iters")
                .short("i")
                .long("iterations")
                .takes_value(true)
                .help("The number of iterations to run the L-system through"),
            Arg::with_name("format")
                .short("f")
                .long("format")
                .takes_value(true)
                .possible_values(&Format::variants())
                .help("Which output format to use"),
            Arg::with_name("angle")
                .short("a")
                .long("angle")
                .takes_value(true)
                .help("The angle to use when drawing the L-system"),
            Arg::with_name("size")
                .short("s")
                .long("size")
                .takes_value(true)
                .min_values(2)
                .max_values(2)
                .help("The size of the canvas to use when drawing"),
        ])
        .get_matches();

    let axiom = matches
        .value_of("axiom")
        .ok_or_else(|| format_err!("Axiom is required!"))?;

    let rules = matches
        .values_of("rules")
        .ok_or_else(|| format_err!("At least one rule is required!"))?;

    let iters = matches.value_of("iters").unwrap_or("1").parse::<usize>()?;

    let angle = matches.value_of("angle").unwrap_or("90").parse::<f64>()?;

    let size = match matches.values_of("size") {
        Some(mut s) => (
            s.next().unwrap().parse::<f64>()?,
            s.next().unwrap().parse::<f64>()?,
        ),
        None => (100., 100.),
    };

    let mut system = LSystem::with_axiom(axiom).map_err(|_| format_err!("Error parsing axiom!"))?;

    for rule in rules {
        system
            .add_parsed_rule(rule)
            .map_err(|_| format_err!("Error parsing rule!"))?;
    }

    let state = system.nth(iters).unwrap();

    match value_t!(matches.value_of("format"), Format).unwrap_or(Format::text) {
        Format::text => println!("{}", state),
        Format::hpgl => {
            let mut hpgl = HPGL::new();
            system
                .render2d(angle.to_radians(), size)
                .rasterize2d(&mut hpgl, "");

            println!("{}", hpgl.result.join("\n"));
        }
    }

    Ok(())
}

fn main() {
    if let Err(e) = run() {
        eprint!("Error: {}", e);
        let mut e = e.as_fail();
        while let Some(cause) = e.cause() {
            eprint!(", {}", cause);
            e = cause;
        }
        eprintln!();
        std::process::exit(1);
    }
}
